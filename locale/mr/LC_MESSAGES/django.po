# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "फाईल प्रकार समर्थित नाही."

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr "विनामूल्य खेळ BOOM"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "अधिक 100000 गेम शोधा"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "शोधा"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "नवीन"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "लोकप्रिय"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "आवडते <br> खेळ"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "शेवटचे <br> खेळले"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "माहिती"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "वर्ग"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "खेळ"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "खेळा"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "हा गेम मोबाइलवर उपलब्ध नाही."

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "नियंत्रणे"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "व्हिडिओ"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "खेळा!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "शीर्ष श्रेण्या"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "मते"

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr "<br> काम करत नाही?"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "जोडले"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "<br> आवडीमध्ये जोडा"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "सामायिक करा"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "पूर्णस्क्रीन"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "यादी साफ करा"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "यानुसार क्रमवारी लावा"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "सर्वात लोकप्रिय"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "टॅग्ज"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "लोकप्रिय टॅग"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "सर्वाधिक लोकप्रिय खेळ"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "लोकप्रिय टॅग्ज"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "सर्वात लोकप्रिय"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "FGM शिफारस केली!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "अधिक %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "शेवटचा खेळला"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "नवीन खेळ"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "नवीन खेळ - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "लोकप्रिय खेळ - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "लोकप्रिय खेळ"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "लोकप्रिय खेळ"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "आवडते खेळ"

#: catalog/views.py:194
msgid "Last Played"
msgstr "शेवटचे खेळलेले"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "सपाट पृष्ठे"

#: flatpages/models.py:12
msgid "Title"
msgstr "शीर्षक"

#: flatpages/models.py:14
msgid "Text"
msgstr "मजकूर"

#: flatpages/models.py:15
msgid "Active"
msgstr "सक्रिय"

#: flatpages/models.py:16
msgid "Modified"
msgstr "सुधारित"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "स्थिर पृष्ठे"

#: flatpages/models.py:20
msgid "Static page"
msgstr "स्थिर पृष्ठ"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "आफ्रिकन लोक"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "अरबी"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "अझरबैजानी"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "बल्गेरियन"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "बेलारशियन"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "बंगाली"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "बोस्नियन"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "कॅटलन"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "झेक"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "वेल्श"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "डॅनिश"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "जर्मन"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "ग्रीक"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "इंग्रजी"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "एस्पेरांतो"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "स्पॅनिश"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "एस्टोनियन"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "फिन्निश"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "फ्रेंच"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "आयरिश"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "गॅलिसियन"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "हिब्रू"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "हिंदी"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "क्रोएशियन"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "हंगेरियन"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "आर्मेनियन"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "इटालियन"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "जपानी"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "जॉर्जियन"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "कझाक"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "ख्मेर"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "कन्नड"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "कोरियन"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "लक्झेंबर्गिश"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "लिथुआनियन"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "लाटवियन"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "मॅसेडोनियन"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "मल्याळम"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "मंगोलियन"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "मराठी"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "बर्मी"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "नेपाळी"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "डच"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "ओसेटिक"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "पंजाबी"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "पोलिश"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "पोर्तुगीज"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "रोमानियन"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "रशियन"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "स्लोव्हाक"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "स्लोव्हेनियन"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "अल्बानियन"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "सर्बियन"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "स्वीडिश"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "स्वाहिली"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "तमिळ"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "तेलगू"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "थाई"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "तुर्की"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "टाटर"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "युक्रेनियन"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "उर्दू"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "व्हिएतनामी"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "सोपी चायनिज"

#: templates/404.html:18
msgid "404 Error"
msgstr "404 त्रुटी"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "अरेरे. हे पृष्ठ अस्तित्वात नाही."

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "अधिक"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "सर्व"

#~ msgid "Indonesian"
#~ msgstr "इंडोनेशियन"

#~ msgid "Kabyle"
#~ msgstr "काबाईल"

#~ msgid "Udmurt"
#~ msgstr "उदमुर्ट"
