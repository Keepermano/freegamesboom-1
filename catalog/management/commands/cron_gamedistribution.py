# -*- coding: utf-8 -*-
import json
from io import BytesIO

import requests
from django.core import files
from django.core.management.base import BaseCommand
from django.db import IntegrityError
from tqdm import tqdm

from catalog.models import Game, Category, Tag, AutoSyncHistory, GamesTranslation


class Command(BaseCommand):

    def handle(self, *args, **options):

        history = AutoSyncHistory()
        history.save()
        total = self.save_items()
        history.message = 'Added: %s' % total
        history.success = True
        history.save()

    @staticmethod
    def save_items():
        total = 0
        mapping = {
            'Girls': Category.objects.get(slug='for-girls'),
            '.IO': Category.objects.get(slug='io-games'),
            'Soccer': Tag.objects.get(slug='soccer-games'),
            'Stickman': Tag.objects.get(slug='stickman-games'),
            'Social': Category.objects.get(slug='adventure-games'),
            'Clicker': Tag.objects.get(slug='clicker'),
            'Multiplayer': Category.objects.get(slug='multiplayer-games'),
            'Farming': Tag.objects.get(slug='farming'),
            'Boys': Tag.objects.get(slug='boys'),
            'Hypercasual': Category.objects.get(slug='action-games'),
            'Cooking': Tag.objects.get(slug='cooking-games'),
            'Arcade': Category.objects.get(slug='arcade-games'),
            '2 Player': Tag.objects.get(slug='2-player-games'),
            'Puzzle': Category.objects.get(slug='puzzle-games'),
            'Racing': Category.objects.get(slug='racing-games'),
            'Adventure': Category.objects.get(slug='adventure-games'),
            'Bejeweled': Category.objects.get(slug='puzzle-games'),
            'Baby': Tag.objects.get(slug='children-games'),
            'Sports': Category.objects.get(slug='sports-games'),
            '3D': Category.objects.get(slug='3d-games'),
            'Shooting': Category.objects.get(slug='shooting-games'),
            'Action': Category.objects.get(slug='action-games')
        }
        for page in range(1, 20):
            r = requests.get(
                'https://catalog.api.gamedistribution.com/api/v1.0/rss/All/?collection=all&categories=All&type=all&mobile=all&rewarded=all&amount=40&page=%d&format=json' % page,
            )
            if not r.status_code == requests.codes.ok:
                print(r.status_code)
            else:
                data = r.json()
                for item in tqdm(data):
                    if Game.objects.filter(name=item['Title']).exists():
                        continue
                    else:
                        try:
                            mapping_obj = mapping[item['Category'][0]]
                        except (IndexError, KeyError):
                            continue
                        game = Game()
                        if isinstance(mapping_obj, Category):
                            game.category = mapping_obj
                        game.name = item['Title']
                        game.service = 2
                        game.link = item['Url']
                        game.mobile = True if item['Mobile'] else False
                        game.save()
                        total += 1

                        try:
                            GamesTranslation.objects.create(
                                text_block_code='description',
                                language_code='en',
                                text=item['Description'],
                                game_id=game.id
                            )
                        except IntegrityError:
                            pass

                        # save image
                        try:
                            img = item['Asset'][0]
                        except (KeyError, IndexError):
                            pass
                        else:
                            resp = requests.get(img)
                            if resp.status_code != requests.codes.ok:
                                pass
                            else:
                                fp = BytesIO()
                                fp.write(resp.content)
                                file_name = img.split("/")[-1]
                                game.image.save(file_name, files.File(fp))

                        # save tag if need
                        if isinstance(mapping_obj, Tag):
                            game.tags.add(mapping_obj)

        return total
