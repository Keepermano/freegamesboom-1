# -*- coding: utf-8 -*-
import random

from django.db import models
from django.db.models import Q, Count
from django.conf import settings
from django.urls import reverse
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.utils.translation import get_language
from django.utils.html import mark_safe

from sorl.thumbnail import ImageField
from autoslug.fields import AutoSlugField
from ckeditor_uploader.fields import RichTextUploadingField
from sortedm2m.fields import SortedManyToManyField
from common.fields import CharTextField

from .utils import upload_dir
from .fields import ContentTypeRestrictedFileField
from .managers import PublicTranslateLanguageManager, PublicLanguageManager


def ___(val):
    return val


GAMES_TEXT_FIELDS = [
    'title', 'h1', 'description', 'control', 'video',
    'awards', 'meta_title', 'meta_desc', 'meta_keywords', 'seo_top'
]


class MetaTagsAbstract(models.Model):
    meta_title = CharTextField(verbose_name='META title', blank=True)
    meta_desc = models.TextField(verbose_name='META description', blank=True)
    meta_keywords = models.TextField(verbose_name='META keywords', blank=True)

    class Meta:
        abstract = True

    @property
    def list_link(self):
        if self.h1:
            return self.h1
        elif self.name:
            return self.name
        return self.name_en


class CategoryTagSorted(models.Model):
    category = models.ForeignKey('catalog.Category', on_delete=models.CASCADE, related_name='source_cat')
    tag = models.ForeignKey('catalog.Tag', on_delete=models.CASCADE, related_name='target_tag')
    order = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Category tag'
        verbose_name_plural = 'Category tags'
        ordering = ('order',)
        unique_together = ('category', 'tag')

    def __str__(self):
        return self.tag.name


class Category(MetaTagsAbstract):

    name = models.CharField(verbose_name=___('Name'), unique=True, max_length=100)
    name2_seo = CharTextField(verbose_name=___('Name2_seo'), blank=True)
    slug = AutoSlugField(verbose_name=___('Slug'), unique=True, max_length=50, populate_from='name', editable=True)
    h1 = CharTextField(verbose_name=___('H1'), blank=True)
    text_link = CharTextField(verbose_name=___('Text for link'), blank=True)
    image = ImageField(verbose_name=___('Image'), upload_to=upload_dir, blank=True)
    top = models.BooleanField(verbose_name=___('Top'), default=False)
    seo_top = RichTextUploadingField(verbose_name=___('SEO top'), blank=True)
    description = RichTextUploadingField(verbose_name=___('Description'), blank=True)
    position = models.IntegerField(verbose_name=___('Position'), default=100, db_index=True)
    public = models.BooleanField(verbose_name=___('Public'), default=True)
    view_on_main = models.BooleanField(verbose_name=___('View on Main'), default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    agame = models.BooleanField(verbose_name=___('AGAME.com'), default=False)

    objects = models.Manager()
    published = PublicLanguageManager()

    tags = models.ManyToManyField('catalog.Tag', through='catalog.CategoryTagSorted', blank=True)
    tags_new = SortedManyToManyField('catalog.Tag', sort_value_field_name='order', related_name='+', blank=True)

    class Meta:
        ordering = ('position', 'name')
        verbose_name = ___('Category')
        verbose_name_plural = ___('Categories')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category-detail', args=(self.slug,))

    @property
    def rating(self):
        val = self.game_set.filter(public=True).aggregate(
            avg_rating=models.Avg('fakelike') / models.Avg('fakedislike'),
        )['avg_rating']
        if val:
            if val >= 5:
                return 5
            else:
                return val
        else:
            return 0

    @property
    def total_votes(self):
        return int(self.game_set.filter(public=True).aggregate(
            total_votes=models.Avg('fakelike') + models.Avg('fakedislike')
        )['total_votes'])

    @property
    def games_published(self):
        return self.game_set(manager='published')


class TagTagSorted(models.Model):
    source_tag = models.ForeignKey('catalog.Tag', on_delete=models.CASCADE, related_name='source')
    target_tag = models.ForeignKey('catalog.Tag', on_delete=models.CASCADE, related_name='target')
    order = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = 'Tag tag'
        verbose_name_plural = 'Tag tags'
        ordering = ('order',)
        unique_together = ('source_tag', 'target_tag')

    def __str__(self):
        return self.target_tag.name


class Tag(MetaTagsAbstract):

    name = models.CharField(verbose_name=___('Name'), unique=True, max_length=100)
    name2_seo = CharTextField(verbose_name=___('Name2_seo'), blank=True)
    slug = AutoSlugField(verbose_name=___('Slug'), unique=True, max_length=50, populate_from='name', editable=True)
    h1 = CharTextField(verbose_name=___('H1'), blank=True)
    alt = CharTextField(blank=True)
    title = CharTextField(blank=True)
    text_link = CharTextField(verbose_name=___('Text for link'), blank=True)
    image = ImageField(verbose_name=___('Image'), upload_to=upload_dir, blank=True)
    priority_category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)
    seo_top = RichTextUploadingField(verbose_name=___('SEO top'), blank=True)
    top = models.BooleanField(verbose_name=___('Top'), default=False)
    description = RichTextUploadingField(verbose_name=___('Description'), blank=True)
    position = models.IntegerField(verbose_name=___('Position'), default=100, db_index=True)
    public = models.BooleanField(verbose_name=___('Public'), default=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    agame = models.BooleanField(verbose_name=___('AGAME.com'), default=False)

    objects = models.Manager()
    published = PublicLanguageManager()

    tags = models.ManyToManyField(
        "self",
        through='catalog.TagTagSorted',
        symmetrical=False,
        blank = True,
        null=True
    )
    tags_new = SortedManyToManyField('self', sort_value_field_name='order', related_name='+', symmetrical=False, blank = True,
        null=True)

    class Meta:
        ordering = ('position', 'name')
        verbose_name = ___('Tag')
        verbose_name_plural = ___('Tags')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('tag-detail', args=(self.slug,))

    @property
    def rating(self):
        val = self.game_set.filter(public=True).aggregate(
            avg_rating=models.Avg('fakelike') / models.Avg('fakedislike'),
        )['avg_rating']
        if val:
            if val >= 5:
                return 5
            else:
                return val
        else:
            return 0

    @property
    def total_votes(self):
        return int(self.game_set.filter(public=True).aggregate(
            total_votes=models.Avg('fakelike') + models.Avg('fakedislike')
        )['total_votes'])

    @property
    def games_published(self):
        return self.game_set(manager='published')


def random_like():
    return (10000 + random.randint(1, 5000)) // 100


def random_dislike():
    return (random.randint(1, 2500)) // 100


class GameTranslationFieldDescriptor:
    def  __init__(self, model):
        self.model = model

    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, instance, owner):
        lang = get_language()

        res = self.model.objects.filter(
            text_block_code=self.name,
            language_code=lang,
            game_id=instance.id
        ).values("text").first()
        if not res:
            self.model.objects.filter(
                text_block_code=self.name,
                language_code=settings.MODELTRANSLATION_DEFAULT_LANGUAGE,
                game_id=instance.id
            ).values("text").first()

        return res['text'] if res is not None else ''


class GamesTranslation(models.Model):
    text_block_code = models.CharField(max_length=100, choices=[(v, v) for v in GAMES_TEXT_FIELDS])
    language_code = models.CharField(max_length=10, choices=settings.LANGUAGES)
    text = models.TextField()
    game = models.ForeignKey('Game', related_name='translations', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('text_block_code', 'language_code', 'game')

    def __str__(self):
        return f"Text block code: '{self.text_block_code}', lang: '{self.language_code}'"


class Game(MetaTagsAbstract):

    category = models.ForeignKey(Category, verbose_name=___('Category'), on_delete=models.CASCADE,
                                 blank=True, null=True)
    name = models.CharField(verbose_name=___('Name'), max_length=150)

    # title = models.CharField(blank=True, max_length=255, null=True)
    # description = models.TextField(blank=True, null=True)
    # control = models.TextField(blank=True, null=True)
    # video = models.TextField(verbose_name=___('video'), blank=True, null=True)
    # awards = RichTextUploadingField(verbose_name=___('awards'), blank=True, null=True)
    # meta_title = CharTextField(verbose_name='META title', blank=True, null=True)
    # meta_desc = models.TextField(verbose_name='META description', blank=True, null=True)
    # meta_keywords = models.TextField(verbose_name='META keywords', blank=True, null=True)
    # h1 = CharTextField(verbose_name=___('H1'), blank=True, null=True)
    # seo_top = RichTextUploadingField(verbose_name=___('SEO top'), blank=True, null=True)

    title = GameTranslationFieldDescriptor(GamesTranslation)
    h1 = GameTranslationFieldDescriptor(GamesTranslation)
    description = GameTranslationFieldDescriptor(GamesTranslation)
    control = GameTranslationFieldDescriptor(GamesTranslation)
    video = GameTranslationFieldDescriptor(GamesTranslation)
    awards = GameTranslationFieldDescriptor(GamesTranslation)
    meta_title = GameTranslationFieldDescriptor(GamesTranslation)
    meta_desc = GameTranslationFieldDescriptor(GamesTranslation)
    meta_keywords = GameTranslationFieldDescriptor(GamesTranslation)
    seo_top = GameTranslationFieldDescriptor(GamesTranslation)

    name2_seo = CharTextField(verbose_name=___('Name2_seo'), blank=True)
    slug = AutoSlugField(verbose_name=___('Slug'), unique=True, max_length=100, populate_from='name', editable=True)
    # title = CharTextField(verbose_name=___('Title'), blank=True)
    # h1 = CharTextField(verbose_name=___('H1'), blank=True)
    tags = models.ManyToManyField(Tag, verbose_name=___('Tags'))
    priority_tag = models.ForeignKey(Tag, verbose_name=___('Priority tag'), blank=True, null=True,
                                     on_delete=models.PROTECT, related_name='priority_games')
    # seo_top = RichTextUploadingField(verbose_name=___('SEO top'), blank=True)

    code = models.TextField(verbose_name=___('HTML code if flash'), blank=True)
    link = models.TextField(verbose_name=___('Iframe link'), blank=True)
    flash = models.FileField(verbose_name=___('Flash file'), blank=True, upload_to=upload_dir)

    # description = RichTextUploadingField(verbose_name=___('info'), blank=True)
    # control = RichTextUploadingField(verbose_name=___('control'), blank=True)
    # video = models.TextField(verbose_name=___('video'), blank=True)
    video_youtube_processed = models.BooleanField(default=False)
    # awards = RichTextUploadingField(verbose_name=___('awards'), blank=True)
    top = models.BooleanField(verbose_name=___('Top'), default=False)
    popular = models.BooleanField(verbose_name=___('Popular'), default=False, editable=False)
    image = ImageField(verbose_name=___('Image'), upload_to=upload_dir, blank=True)
    image_big = ImageField(verbose_name=___('Image Banner'), upload_to=upload_dir, blank=True)
    slider_color = models.CharField(verbose_name=___('Color (slider)'), max_length=6, blank=True, default='42605e')
    position = models.IntegerField(verbose_name=___('Position'), default=100, db_index=True)
    last_open_date = models.DateTimeField(editable=False, null=True, blank=True)
    insecure = models.BooleanField(verbose_name=___('No SSL Page'), default=False)
    public = models.BooleanField(verbose_name=___('Public'), default=True)
    mobile = models.BooleanField(verbose_name=___('Mobile'), default=False)
    agame = models.BooleanField(verbose_name=___('AGAME.com'), default=False, editable=False)
    service = models.IntegerField(
        choices=[
            (1, 'agame.com'),
            (2, 'gamedistribution.com'),
        ],
        blank=True,
        null=True
    )
    type = models.IntegerField(
        choices=[
            (1, 'html'),
            (2, 'flash'),
        ],
        default=1
    )
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    modified = models.DateTimeField(auto_now=True)

    play_counter = models.PositiveIntegerField(default=0, db_index=True)
    play_counter_mobile = models.PositiveIntegerField(default=0, db_index=True)

    fakelike = models.PositiveIntegerField(default=random_like)
    fakedislike = models.PositiveIntegerField(default=random_dislike)
    like = models.PositiveIntegerField(default=0)
    dislike = models.PositiveIntegerField(default=0)

    doesnt_work = models.PositiveIntegerField(verbose_name='Doesn’t work', default=0)

    is_translate = models.BooleanField(default=False, editable=False)

    objects = models.Manager()
    published = PublicLanguageManager()

    class Meta:
        ordering = ('position', 'name')
        verbose_name = ___('Game')
        verbose_name_plural = ___('Games')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        if self.insecure and not settings.DEBUG:
            return reverse('game-detail-insecure', args=(self.slug,))
        else:
            return reverse('game-detail', args=(self.slug,))

    def get_absolute_url_full(self):
        if self.insecure and not settings.DEBUG:
            return reverse('game-detail-insecure-full', args=(self.slug,))
        else:
            return reverse('game-detail-full', args=(self.slug,))

    @property
    def scheme(self):
        if self.insecure or settings.DEBUG:
            return 'http'
        else:
            return 'https'

    @property
    def get_video(self):
        if self.video:
            return self.video.split('\n')

    @property
    def rating(self):
        try:
            val = 5 * (self.fakelike / (self.fakelike + self.fakedislike))
        except ZeroDivisionError:
            return 0
        return round(val, 1)

    @property
    def total_votes(self):
        return self.fakelike + self.fakedislike

    @property
    def embed_code(self):
        if self.code:
            return mark_safe(self.code)
        elif not self.flash:
            return mark_safe('<iframe src="{}" width="100%" height="100%"></iframe>'.format(self.link))
        else:
            link = self.flash.url
            return mark_safe('<object width="100%" height="100%">'
                             '<param name="movie" value="{link}">'
                             '<param name="wmode" value="direct" />'
                             '<embed src="{link}"  width="100%" height="100%" '
                             'allownetworking="internal" wmode="direct" type="application/x-shockwave-flash" />'
                             '</object>'.format(link=link))


class GameVideo(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    code = models.TextField()
    position = models.IntegerField(verbose_name=___('Position'), default=100, db_index=True)

    class Meta:
        ordering = ('position', )
        verbose_name = ___('Video')
        verbose_name_plural = ___('Video')


class GameParse(models.Model):
    title = models.CharField(max_length=250, blank=True)
    slug = models.SlugField(unique=True)
    url = models.CharField(max_length=250, blank=True)
    image = models.CharField(max_length=250, blank=True)
    category = models.CharField(max_length=250, blank=True)
    subcategory = models.CharField(max_length=250, blank=True)
    description = models.TextField(blank=True)
    tags = models.TextField(blank=True)
    code = models.TextField(blank=True)
    code_clear = models.CharField(blank=True, max_length=250)


class GameErrorParse(models.Model):
    url = models.CharField(max_length=250, unique=True)


class Block(models.Model):
    TYPES = (
        (1, 'horizontal'),
        (2, 'vertical'),
    )
    type = models.IntegerField(choices=TYPES, default=1)
    name = CharTextField()
    link = models.CharField(max_length=100, blank=True)
    position = models.IntegerField(verbose_name=___('Position'), default=100, db_index=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        lang = get_language()
        if not lang == 'en':
            return '/{lang}{link}'.format(lang=lang, link=self.link)
        return self.link


class BlockTag(models.Model):
    block = models.ForeignKey(Block, on_delete=models.CASCADE, related_name='items')
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)
    position = models.IntegerField(verbose_name=___('Position'), default=100, db_index=True)


class TranslateLanguage(models.Model):
    slug = models.CharField(max_length=10, choices=[(x[0], x[0]) for x in settings.LANGUAGES], unique=True,
                            help_text='ISO 639-1', verbose_name='Language code')
    locale = models.CharField(max_length=10, blank=True)
    country_code = models.CharField(max_length=2, blank=True, help_text='ISO 3166-1 alpha-2')
    name = models.CharField(max_length=50)
    processed = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    position = models.IntegerField(verbose_name=___('Position'), default=100)

    objects = models.Manager()
    public = PublicTranslateLanguageManager()

    @property
    def code(self):
        return self.slug

    class Meta:
        ordering = ('-processed', 'position',  'slug')

    def __str__(self):
        return self.name


class AutoSyncHistory(models.Model):

    date_start = models.DateTimeField(auto_now_add=True)
    date_end = models.DateTimeField(auto_now=True)
    success = models.BooleanField(default=False)
    message = models.TextField(blank=True)

    @property
    def duration(self):
        return naturaltime(self.date_end - self.date_start)

    class Meta:
        verbose_name = 'Sync history'
        verbose_name_plural = 'Sync history'


class Import(models.Model):

    file = ContentTypeRestrictedFileField(
        upload_to=upload_dir,
        content_types=['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
    )
    date_start = models.DateTimeField(auto_now_add=True)
    date_end = models.DateTimeField(auto_now=True)
    in_processed = models.BooleanField(default=False, editable=False)
    success = models.BooleanField(default=False)
    message = models.TextField(blank=True)

    @property
    def duration(self):
        return naturaltime(self.date_end - self.date_start)

    class Meta:
        verbose_name = 'Import'
        verbose_name_plural = 'Import'
