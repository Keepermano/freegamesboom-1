from django.conf import settings
from django.http import Http404, HttpResponse, HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404
from django.template import loader, RequestContext
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_protect

from .models import FlatPage


DEFAULT_TEMPLATE = 'flatpages/default.html'


def flatpage(request, url):
    """
    Public interface to the flat page view.

    Models: `flatpages.flatpages`
    Templates: Uses the template defined by the ``template_name`` field,
        or :template:`flatpages/default.html` if template_name is not defined.
    Context:
        flatpage
            `flatpages.flatpages` object
    """
    if not url.startswith('/'):
        url = '/' + url

    language = request.LANGUAGE_CODE
    language_prefix = '/%s' % language

    if url.startswith(language_prefix):
        url = url[len(language_prefix):]

    try:
        f = get_object_or_404(FlatPage, url__exact=url)
    except Http404:
        if not url.endswith('/') and settings.APPEND_SLASH:
            url += '/'
            f = get_object_or_404(FlatPage, url__exact=url)
            return HttpResponsePermanentRedirect('%s/' % request.path)
        else:
            raise
    return render_flatpage(request, f)


@csrf_protect
def render_flatpage(request, f):
    """
    Internal interface to the flat page view.
    """
    template = loader.get_template(DEFAULT_TEMPLATE)
    f.title = mark_safe(f.title)
    f.text = mark_safe(f.text)
    response = HttpResponse(template.render({'flatpage': f}, request))
    return response
