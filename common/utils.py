# -*- coding: utf-8 -*-
import hashlib
import time

from django.contrib.contenttypes.models import ContentType
from modeltranslation.utils import get_language


def upload_dir(instance, filename):
    tmp = '%s:%s' % (instance, str(time.time()))
    m = hashlib.md5(tmp.encode('utf-8')).hexdigest()
    ctype = ContentType.objects.get_for_model(instance)
    model = ctype.model
    return 'upload/%s/%s/%s.%s' % (model, m[:2], m, filename.split('.')[-1])


class TranslationFieldDescriptor:
    def __init__(self, model):
        self.model = model

    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, instance, owner):
        lang = get_language()

        res = self.model.objects.filter(
            text_block_code=self.name,
            language_code=lang
        ).values("text").first()

        return res['text'] if res is not None else ''
