from django.db.models import TextField


class CharTextField(TextField):
    def formfield(self, **kwargs):
        return super().formfield(**kwargs)
